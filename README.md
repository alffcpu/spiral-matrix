# spiral-matrix

matrix.js - ES6 Class  
display_matrix - ES6 function  

Demo:  
----  
http://jsfiddle.net/zu9kmy84/12/  

Дано:  
---  
n — целое >= 1  

Требуется:  
---  
1) Сгенерировать матрицу со сторонами 2n-1 заполненную произвольными числами (или натуральным рядом).  
2) Вывести все числа по порядку, пройдя из центра по спирали против часовой стрелки.  

Пример матрицы (для n = 2):  
https://yadi.sk/i/Jn3ngoVG3YBbi6  

Язык — Javascript.  