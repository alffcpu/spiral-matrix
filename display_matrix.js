function displayMartix (nValue, minRandom = 1, maxRandom = 1000) {

	const pow = Math.pow(2, nValue);
	const size = pow - 1;
	const center = Math.floor(pow / 2) - 1;

	const matrix = [];
	for (let row = 0; row < size; row++) {
		matrix[row] = [];
		for (let cell = 0; cell < size; cell++) {
			matrix[row][cell] = Math.round(Math.random() * (maxRandom - minRandom) + minRandom);
		}
	}

	let x = center;
	let y = center;

	let direction = 0;
	let chainSize = 1;
	let counter = 0;

	const commands = [[0, -1], [1, 0], [0, 1], [-1, 0]];

	const result = [];
	for (let k = 1; k <= (size - 1); k++) {
		for (let j = 0; j < (k < (size - 1) ? 2 : 3); j++) {
			for (let i = 0; i < chainSize; i++) {
				result.push(matrix[x][y]);
				counter++;
				[addX, addY] = commands[direction];
				x += addX;
				y += addY;
			}
			direction = (direction + 1) % 4;
		}
		chainSize = chainSize + 1;
	}
	result.push(matrix[x][y]);

	const padStringLength = ("" + (maxRandom - 1)).length;
	const padString = Array(padStringLength + 1).join(' ');

	matrix.forEach((row) => {
		console.log(row.map((value) => (padString + value).slice(-padStringLength)).join(' '));
	});
	console.log("\n" + result.join(','));

};