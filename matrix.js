class Matrix {
	/**
	 * @param nValue
	 * @param options
	 */
	constructor(nValue = 2, options = {}) {

		const {minRandom = 1, maxRandom = 1000} = options;

		const pow = Math.pow(2, nValue);
		const size = pow - 1;
		const center = Math.floor(pow / 2) - 1;

		this.params = {minRandom, maxRandom, pow, size, center};
		this.matrix = [];
		this.spiral = [];
	}

	/**
	 * Creates 2n-1 matrix
	 * @returns {Matrix}
	 */
	create() {
		const {matrix, params} = this;
		const {size, maxRandom, minRandom} = params;

		for (let row = 0; row < size; row++) {
			matrix[row] = [];
			for (let cell = 0; cell < size; cell++) {
				matrix[row][cell] = Math.round(Math.random() * (maxRandom - minRandom) + minRandom);
			}
		}
		return this;
	}

	/**
	 * Makes spiral
	 * 0=left, 1=down, 2=right, 3=up
	 * @param commands
	 */
	unspin( commands = [[0, -1], [1, 0], [0, 1], [-1, 0]] ) {

		const {size, center} = this.params;
		const {matrix, spiral} = this;

		let x = center;
		let y = center;

		let direction = 0;
		let chainSize = 1;
		let counter = 0;

		for (let k = 1; k <= (size - 1); k++) {
			for (let j = 0; j < (k < (size - 1) ? 2 : 3); j++) {
				for (let i = 0; i < chainSize; i++) {
					spiral.push(matrix[x][y]);
					counter++;
					const [addX, addY] = commands[direction];
					x += addX;
					y += addY;
				}
				direction = (direction + 1) % 4;
			}
			chainSize = chainSize + 1;
		}
		spiral.push(matrix[x][y]);

		return this;
	}

	/**
	 * Displays siral in console
	 * @returns {Matrix}
	 */
	display() {
		const {matrix, spiral, params} = this;
		const {maxRandom} = this.params;
		const padStringLength = ("" + (maxRandom - 1)).length;
		const padString = Array(padStringLength + 1).join(' ');

		matrix.forEach((row) => {
			console.log(row.map((value) => (padString + value).slice(-padStringLength)).join(' '));
		});
		console.log("\n" + spiral.join(','));
		return this;
	}
}